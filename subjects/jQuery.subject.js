import {Subject} from './subject';

class JQuerySubject extends Subject {
    constructor() {
        super();
        let counter = 0;
        (function waitForJQuery(that){
            if(!window.jQuery){
                counter++;
                setTimeout(function(){
                    waitForJQuery(that);
                },25);
            } else {
                console.log(' jQuery loaded after ', counter, ' 25ms reapets');
                that.notify(jQuery);
            }
        })(this);

        // if (!window.jQuery) {
        //     Object.defineProperty(window, "jQuery", {
        //         set: (value) => {
        //             window.setTimeout(() => {
        //                 this.notify(jQuery);
        //             }, 0);
        //             Object.defineProperty(window, "jQuery", {
        //                 value: value
        //             });
        //         },
        //         configurable: true
        //     });
        // } else {
        //     this.notify(jQuery);
        // }
    }

    notify(jQuery){
        this.jQuery = jQuery;
        super.notify(this.jQuery);
    }

    subscribe(observer){
        if (this.jQuery){
            observer(this.jQuery);
        }
        super.subscribe(observer);
    }
}

export const jQuerySubject = new JQuerySubject();