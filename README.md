### How run growcode commad line helper project ###

You will need to do couple of things: 

1. Make npm install on root folder

2. Go to root folder and execute "npm run build-link" (it will build  project with all dependency into /dist folder)

3. Check if you have access to "growcode" command in your local prompt. 

4. Run growcode --help to check available commands and options

## How to run swiftswap script in develop mode ##
You will need to do couple of things : 
1. Make sure that you have packakge.json file in your project root with this content :

    {
        "name": "develop_mode",
        "version": "1.0.0",
        "description": "",
        "scripts": {
          "prepare-develop-mode": "npm install -g live-server && npm install --save live-server-https",
          "develop-mode": "live-server --https=node_modules/live-server-https  magic.js"
        },
        "author": "conversion",
        "license": "ISC",
        "dependencies": {
          "live-server-https": "0.0.2"
        }
      }
2. Go to the root directory and run "npm run prepare-develop-mode"
3. (Optional) If you want to watch changes all the time run "growcode watch [versionName]" otherwise run "growcode build [versionName]"
4. Create a new version or use the existing one for example use the latest version in swiftswap app (you will need to do this only once)
5. Paste this function in selected version in index.js file

        import { LoadLibraries } from '../../utils/load-libraries';
        import libs from '../../lib/libs.dict';

        export default function run() {
            if (!window.isConversionProdModeLoaded) {
                LoadLibraries.loadOne(libs.DevConversion).then(() => {        
                    console.log("Conversion Script Loaded Successfully");
                });   
            }
            window.isConversionProdModeLoaded = true;
        }

6. Deploy the selected version with function above to production "growcode send vX false"
7. Go to conversion app download updated version and go to the preview mode.
8. Bind your 127.0.0.1 to the local domain name dev.conversion.com (Edit hosts file in your system for linux nano /etc/hosts for windows SystemRoot%\system32\drivers\etc\hosts )



127.0.0.1           dev.conversion.com




9. Save edited hosts file go to https://dev.conversion.com:8080 and accept untrusted connection to your local server.
10. Go to the page you should see your local watched changes from your machine, without any addons :-)






