import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';
import callInsertElement from '../../../utils/callInsert';

import UspTemplate from './usp-template.html';

class UspModifier extends ModifierSubject {
    constructor() {
        super('body', (that) => {});
    }

    applyStyles() {
        require('./usp-modifiermodifier.scss');
    }

    applyShoesStyles(){
        require('./usp-modifiermodifier.scss');
        require('./shoes-styles.scss');
    }

    addUspElement(selector = '.top-wrapper') {
        return awaitElements(selector).then((elem) => {
            callInsertElement(elem, jQuery(UspTemplate), 'prepend');
        })
    }

}

const uspModifierbject = new UspModifier();
export default uspModifierbject;