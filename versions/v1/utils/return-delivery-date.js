import DeliveryDataCompute from "../../../utils/getDeliveryDay";

export function getDeliveryDate(dayStart = 2, dayEnd = 4) {
    const deliveryDateCompute = new DeliveryDataCompute();
    const eDate = deliveryDateCompute.skipWorkingDays(dayStart);
    const lDate = deliveryDateCompute.skipWorkingDays(dayEnd);
    return `Zamów teraz, wysyłka 24h, u Ciebie już ${eDate.day}${(eDate.month==lDate.month)?'' : '.'+eDate.month}-${lDate.day}.${lDate.month}`;
}