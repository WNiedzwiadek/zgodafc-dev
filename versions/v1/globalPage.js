import uspModifierbject from './usp-modifier/usp-modifiermodifier';
import kpModifierbject from './kp-modifier/kp-modifier';
import basketObject from './basket/basket-modifier';
import pathChecked from '../../utils/pathChecker';

export default function runGlobalPage() {
    if (window.location.pathname == '/') {
        uspModifierbject.subscribe(() => {
            uspModifierbject.addUspElement();
            uspModifierbject.applyStyles();
        })
    }

    kpModifierbject.subscribe(() => {
        kpModifierbject.applyStyles();
        kpModifierbject.applyChanges();
            uspModifierbject.addUspElement();
            uspModifierbject.applyStyles();
    })
    
    pathChecked(/basket/,()=>{
        uspModifierbject.addUspElement();
        uspModifierbject.applyStyles();
        basketObject.applyChanges();
        basketObject.applyStyles();
    })

    pathChecked(/menu/,()=>{
        uspModifierbject.addUspElement();
        uspModifierbject.applyShoesStyles();
    })

    pathChecked(/rodzaje-podeszw/,()=>{
        uspModifierbject.addUspElement();
        uspModifierbject.applyShoesStyles();
    })
}