import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';
import callInsertElement from '../../../utils/callInsert';

import PromoBadge from './promo-badge.html';
import { getDeliveryDate } from '../utils/return-delivery-date';

class KPModifier extends ModifierSubject {
    constructor() {
        super('.product', (that) => {});
    }

    applyChanges() {
        this.selectSizeByDefault();
        this.addPromoTopBadge();
        this.moveProductSizes();
        this.addDeliveryInfo();
    }

    applyStyles() {
        require('./kp-modifier.scss');
    }

    addPromoTopBadge() {
        return Promise.all([
            awaitElements('.product__body .product__percent'),
            awaitElements('.product .tab-content')
        ]).then(([percent, elementRef]) => {
            elementRef.before(PromoBadge);
        }).catch(()=>{
            //log no promo padge for this item
        });
    }

    selectSizeByDefault() {
        return awaitElements('.size >ul >li:first-of-type').then((elem) => {
            elem.trigger('click');
        });
    }

    moveProductSizes() {
        return Promise.all([
            awaitElements('.product__size .product__size-text'),
            awaitElements('.product__size > .size > p')
        ]).then(([tableSizeElem, choseSizeText]) => {
            const sizeTable = jQuery(`<span class="jbTableLink">Tabela Rozmiarów</span>`);
            sizeTable.click((e) => {
                tableSizeElem.trigger('click')
            });
            choseSizeText.append(sizeTable);
        });
    }

    addDeliveryInfo(){
        awaitElements('.product__action').then((elem)=>{
            elem.append(`<span>${getDeliveryDate()}!</span>`);
        })
    }
}

const kpModifierbject = new KPModifier();
export default kpModifierbject;