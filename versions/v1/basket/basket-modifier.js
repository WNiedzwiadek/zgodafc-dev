import { ModifierSubject } from '../../../modifiers/modifierSubject'
import { awaitElements } from '../../../utils/elements';
import { getDeliveryDate } from '../../v1/utils/return-delivery-date';
import { executeScriptWithTag } from '../../../utils/executeScript';
import { findProperScriptTagWithDatalayer } from '../../../utils/parse-script-tag-products';
import guard from '../../../utils/guard';
import { ajaxSubject } from '../../../utils/ajax.subject';

class KpModifier extends ModifierSubject {
    constructor() {
        super('.basket', (that) => {})
    }
    applyChanges() {
        this.findProperScript().then(({ products }) => {
            this.addPriceInfoToDiscountProducts(products).then(response => {
                this.addStaticText(response);
            });
        });
        this.addDeliveryInfo();
        this.watchBasketUpdate();
    }

    applyStyles() {
        require('./basket.scss');
    }

    watchBasketUpdate() {
        ajaxSubject.subscribe((url) => {
            if (/basket\/qty/.test(url)) {
                setTimeout(() => {
                    this.addPriceInfoToDiscountProducts(window.productInBasket.products, true);
                    this.addDeliveryInfo();
                    this.addFreeDeliveryInfo();
                });
            }
        });
    }

    findProperScript() {
        return new Promise((resolve) => {
            findProperScriptTagWithDatalayer((productArrayString) => {
                executeScriptWithTag(productArrayString).then(() => {
                    resolve(window.productInBasket);
                })
            });
        })
    }

    addStaticText(isPromoProduct) {
        return awaitElements('.basket .basket__title.upper').then(elem => {
            const totalPrice = this.addFreeDeliveryInfo();
            if (!isPromoProduct) {
                elem.after(`<span class="jbStaticText jOneLine">Dodanie produktów do koszyka nie oznacza ich rezerwacji!</span>`)
            } else {
                elem.after(`<span class="jbStaticText">Dodanie produktów do koszyka nie oznacza ich rezerwacji!<br> W Twoim koszyku są produkty promocyjne - nie przegap niższej ceny!</span>`);
            }
        });
    }

    addFreeDeliveryInfo() {
        const totalPrice = jQuery('.summary__final-price span').text().replace(",", ".").replace(/[^0-9\.]+/g, "");
        
        if (totalPrice >= 200.00) {
            jQuery('.summary__delivery-price span').text('DARMOWY');
        }else{
            jQuery('.summary__delivery-price span').text('OD 9 PLN');
        }
        return totalPrice;
    }

    addPriceInfoToDiscountProducts(productInBasket = [], isUpdate = false) {
        return awaitElements('.col-xs-12.basket_item.window').then(elems => {
            let isDiscountProductOrFreeDelivery = false;
            jQuery(productInBasket).each((i, obj) => {
                const coupon = obj.coupon;
                if (coupon) {
                    isDiscountProductOrFreeDelivery = true;
                    const quantity = Number(jQuery(elems[i]).find('.qty__input').val());
                    let oldPrice = (obj.dimension4 * quantity).toFixed(2);
                    if (isUpdate) {
                        jQuery(elems[i]).find('.jbPriceOld').text(`${oldPrice} PLN`);
                        jQuery(elems[i]).find('.jbDiscount').text(`${coupon}% taniej`);
                    } else {
                        jQuery(elems[i]).find('.data-total').before(`<span class=jbPriceOld>${oldPrice} PLN</span>`);
                        jQuery(elems[i]).find('.data-total').parent().append(`<span class=jbDiscount>${coupon}% taniej</span>`);
                    }
                }
            });
            return isDiscountProductOrFreeDelivery;
        });
    }

    addDeliveryInfo() {
        awaitElements('.summary-info .row .col-md-12.col-sm-5 .btn.btn-inverse').then((elem) => {
            elem.after(`<span class=jbDelivery>${getDeliveryDate()}!</span>`);
        })
    }
}
const kpModifierObject = new KpModifier();
export default kpModifierObject;