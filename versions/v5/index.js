import {LoadingScreen} from "../../utils/loading-screen";
import {Device} from "../../utils/device";
import desktopRunGlobalPage from "./desktop/desktopGlobalPage";
import mobileRunGlobalPage from './mobile/mobileGlobalPage';

export default function run(){
    let desktopDevice = new Device();
    let mobileDevice = new Device();
    desktopDevice.setAllowed('desktop');
    mobileDevice.setAllowed('mobile');
    window.runningVersionSSarr = window.runningVersionSSarr || [];
    if (!~window.runningVersionSSarr.indexOf('v5')){
        console.log("dziala wersja v5");
        window.runningVersionSS = 'v5'; //for page quasii full reloading checking
        window.runningVersionSSarr.push('v5');
        const ls = new LoadingScreen();

        if(desktopDevice.isValid()){
            desktopRunGlobalPage();
        }
        if(mobileDevice.isValid()){
            mobileRunGlobalPage();
        }
    }
}