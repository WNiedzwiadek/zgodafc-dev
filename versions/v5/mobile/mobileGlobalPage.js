import basketObject from '../../v4/basket/basket-modifier';
import loginObject from '../../v4/login/login-modifier';
import orderObject from '../../v4/order/order-modifier';
import pathChecker from '../../../utils/pathChecker';
import confirmObject from '../../v4/confirm/confirm-modifier';
import deliveryObject from '../../v4/delivery/delivery-modifier';
import kpObject from '../../v4/kp/kp-modifier';

export default function runGlobalPage() {

    kpObject.subscribe(() => {
        kpObject.applyStyles();
        kpObject.applyChanges();
    })

    basketObject.subscribe(() => {
        basketObject.applyChanges();
        basketObject.applyStyles();
    })

    pathChecker(/login/, () => {
        loginObject.subscribe(() => {
            loginObject.applyChanges();
            loginObject.applyStyles();
        });
    })

    pathChecker(/order/, () => {
        orderObject.subscribe(() => {
            orderObject.applyChanges();
            orderObject.applyStyles();
        })
    });

    pathChecker(/order\/delivery/, () => {
        deliveryObject.applyChanges();
        deliveryObject.applyStyles();
    });

    pathChecker(/order\/confirm/, () => {
        confirmObject.subscribe(() => {
            confirmObject.applyChanges();
            confirmObject.applyStyles();
        })
    });

}