import BasketObject from '../../v2/basket/basket-modifier'
import LoginObject from '../../v2/login/login-modifier';
import OrderObject from '../../v2/order/order-modifier';
import pathChecker from '../../../utils/pathChecker';
import kpModifierbject from '../../v2/kp-modifier/kp-modifier';
import DeliveryObject from '../../v2/delivery/delivery-modifier';
import ConfirmObject from '../../v2/confirm/confirm-modifier';

export default function runGlobalPage() {

    pathChecker(/basket/, () => {
        BasketObject.subscribe(() => {
            BasketObject.applyChanges();
            BasketObject.applyStyles();
        });
    })

    pathChecker(/login/, () => {
        LoginObject.subscribe(() => {
            LoginObject.applyChanges();
            LoginObject.applyStyles();
        });
    })

    pathChecker(/order/, () => {
        OrderObject.subscribe(() => {
            OrderObject.applyChanges();
            OrderObject.applyStyles();
        })
    });

    kpModifierbject.subscribe(() => {
        kpModifierbject.applyStyles();
        kpModifierbject.applyChanges();
    })

    pathChecker(/order\/delivery/, () => {
        DeliveryObject.applyChanges();
        DeliveryObject.applyStyles();
    });

    pathChecker(/order\/confirm/, () => {
        ConfirmObject.applyChanges();
        ConfirmObject.applyStyles();
    });
}