import { ModifierSubject } from '../../../modifiers/modifierSubject'
import { awaitElements } from '../../../utils/elements';
import { getDeliveryDate } from '../../v1/utils/return-delivery-date';
import { executeScriptWithTag } from '../../../utils/executeScript';
import { findProperScriptTagWithDatalayer } from '../../../utils/parse-script-tag-products';
import guard from '../../../utils/guard';
import { ajaxSubject } from '../../../utils/ajax.subject';
import HeaderObject from '../header/header-modifier';
import FooterObject from '../footer/footer-modifier';

class LoginModifier extends ModifierSubject {
    constructor() {
        super('.basket', (that) => {})
    }

    applyChanges() {
        HeaderObject.addHeader();
        HeaderObject.addHeaderText('LOGOWANIE');
        HeaderObject.addSSL();
        HeaderObject.applyStyles();

        FooterObject.addFooter();
        FooterObject.applyStyles();

        this.addStaticLoginText();
        this.showLoginSection();
        this.facebookLogin();
        this.addStaticText();
    }

    applyStyles() {
        require('./login.scss');
    }

    showLoginSection(){
        return awaitElements('.login-section').then(elem => {
            const content = jQuery(`<div class=jbLoginContent><span>Masz już konto?</span><span class=jbLoginContentClick>Zaloguj się tutaj</span></div>`);

            content.find('.jbLoginContentClick').click(() => {
                jQuery('.login-section').css('display', 'flex');
                jQuery('.jbLoginContent').css('display', 'none');
            });
            elem.before(content);
        });
    }

    addStaticLoginText(){
        return awaitElements('.login-section__inputs .page-header').then(elem => {
            elem.after(`<h3 class="page-header jbHeader">Zaloguj się</h3>`);
        });
    }

    addStaticText(){
        return awaitElements('.col-sm-12 .login-section__nologin .btn.btn-inverse').then(elem => {
            elem.before(`<span>SZYBKIE ZAKUPY BEZ REJESTRACJI</span>`);
        })
    }

    facebookLogin(){
        return awaitElements('.facebook').then(elem => {
            const content = elem.clone(true, true).find('a').text('ZALOGUJ SIĘ PRZEZ FACEBOOKA');
            elem.before(content.addClass('jbFb'));
        });
    }
}
const loginModifierObject = new LoginModifier();
export default loginModifierObject;