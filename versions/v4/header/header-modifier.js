import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';
import HeaderTemplate from './header-template.html';
import SSLTemplate from './ssl-template.html';

class HeaderModifier extends ModifierSubject {
    constructor() {
        super('.login_page.container', (that) => {});
    }

    applyStyles() {
        require('./header.scss');
    }

    addHeader(){
        return awaitElements('.nav').then(elem => {
            elem.before(HeaderTemplate);
        });
    }

    addSSL(){
        return awaitElements('#jbHeader').then(elem => {
            elem.before(SSLTemplate);
        })
    }

    addHeaderText(text) {
        return awaitElements('.container-fluid.down_margin').then(elem => {
            elem.before(`<span class=jbHeaderText>${text}</span>`);
        });
    }
}
const headerModifier = new HeaderModifier();
export default headerModifier;