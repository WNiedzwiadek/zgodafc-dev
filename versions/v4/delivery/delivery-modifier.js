import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';
import callInsertElement from '../../../utils/callInsert';
import HeaderObject from '../header/header-modifier';
import FooterObject from '../footer/footer-modifier';

class DeliveryModifier extends ModifierSubject {
    constructor() {
        super('.mainDelivery', (that) => {});
    }

    applyChanges() {
        this.addNewDeliveryTitleText().then(() => {
            HeaderObject.addHeader();
            HeaderObject.addSSL();
            HeaderObject.applyStyles();
            FooterObject.addFooter();
            FooterObject.applyStyles();
            this.eventsDelivery();
            this.eventsPayments();
            this.addNewDeliveryHeader();
            this.addNewPaymentsTitleText();
            this.insertDeliveryHeaders();
            this.insertPaymentsWhiteSpace();
            this.handleDeliveriesVisibility();
        })
    }

    applyStyles() {
        require('./delivery.scss');
    }
    addNewDeliveryTitleText() {
        return awaitElements('.top-bar__bg').then((elem) => {
            elem.after(`
            <div class="row jbNewDeliveryTitle">
                <div class="container">
                    <h2>WYBÓR DOSTAWY I PŁATNOŚCI</h2>
                </div>
            </div>`);
        })
    }

    addNewDeliveryHeader() {
        return awaitElements('.row.deliveries').then(elem => {
            elem.before(`<h3 class=jbDeliveryHeader>WYBIERZ METODĘ DOSTAWY</h3>`);
        });
    }

    addNewPaymentsTitleText() {
        return awaitElements('.row.payments').then(elem => {
            elem.before(`<h3 class=jbPaymentsHeader>WYBIERZ METODĘ PŁATNOŚCI:</h3>`);
        });
    }

    insertDeliveryHeaders() {
        return awaitElements('.row.deliveries label').then(elem => {
            elem.each((index, object) => {
                const obj = this.jQuery(object);
                obj.find('img').after(`<span class=jbWhiteSpace></span>`);
            })
        });
    }

    insertPaymentsWhiteSpace() {
        return awaitElements('.row.payments label').then(elem => {
            elem.each((index, object) => {
                jQuery(object).prepend(`<span class=jbWhiteSpace></span>`);
            })
        });
    }

    eventsDelivery() {
        return awaitElements('.row.deliveries label').then(elem => {
            elem.click((e) => {
                const object = this.jQuery(e.currentTarget);

                this.jQuery('.jbDeliveryActive').removeClass('jbDeliveryActive');
                object.addClass('jbDeliveryActive');
            })
        });
    }

    eventsPayments() {
        return awaitElements('.row.payments label').then(elem => {
            elem.click((e) => {
                const object = this.jQuery(e.currentTarget);
                this.jQuery('.jbPaymentsActive').removeClass('jbPaymentsActive');
                object.addClass('jbPaymentsActive');
            })
        });
    }


    handleDeliveriesVisibility() {
        return awaitElements('.deliveries')
            .then((elem) => {
                elem.find('.deliveries__row:nth-of-type(2)').addClass('jbHidden');
                const tabs = jQuery(`
            <div class="row jbDeliveriesTab">
                <div class="jbDeliveriesTabBefore active"><span>Płatność przed wysyłką</span></div>
                <div class="jbDeliveriesTabAfter"><span>Płatność przy odbiorze</span></div>
            </div>`);
                tabs.find('.jbDeliveriesTabBefore').click((e) => {
                    this.handleCLick(e, '2');
                })
                tabs.find('.jbDeliveriesTabAfter').click((e) => {
                    this.handleCLick(e, '1');
                })
                elem.before(tabs);
            });
    }

    handleCLick(e, nthOfType = '1') {
        jQuery('.jbDeliveriesTab > *').removeClass('active');
        jQuery(e.currentTarget).addClass('active');
        jQuery('.deliveries__row').removeClass('jbHidden');
        jQuery(`.deliveries__row:nth-of-type(${nthOfType})`).addClass('jbHidden');
    }
}

const deliveryModifierbject = new DeliveryModifier();
export default deliveryModifierbject;