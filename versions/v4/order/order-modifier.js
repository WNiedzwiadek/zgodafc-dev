import { ModifierSubject } from '../../../modifiers/modifierSubject'
import { awaitElements } from '../../../utils/elements';
import { getDeliveryDate } from '../../v1/utils/return-delivery-date';
import { executeScriptWithTag } from '../../../utils/executeScript';
import { findProperScriptTagWithDatalayer } from '../../../utils/parse-script-tag-products';
import guard from '../../../utils/guard';
import { ajaxSubject } from '../../../utils/ajax.subject';
import HeaderObject from '../header/header-modifier';
import FooterObject from '../footer/footer-modifier';

class OrderModifier extends ModifierSubject {
    constructor() {
        super('.order-address', (that) => {})
    }

    applyChanges() {
        HeaderObject.addHeader();
        HeaderObject.addHeaderText('DANE DO WYSYŁKI');
        HeaderObject.addSSL();
        HeaderObject.applyStyles();

        FooterObject.addFooter();
        FooterObject.applyStyles();

        this.changeReqCheckboxText();
        this.copyButtons();
    }

    applyStyles() {
        require('./order.scss');
    }

    changeReqCheckboxText(){
        return awaitElements('label[for="agree"]').then(elem => {
            elem.html('(WYMAGANE) Zapoznałam/em się z <a href="https://www.zgodafc.pl/content/regulamin,132.html" target="_blank">Regulaminem sklepu internetowego</a> i akceptuję jego postanowienia.<br>');
        })
    }

    copyButtons(){
        return awaitElements('#form-order > div:nth-child(9)').then(elem => {
            const content = elem.clone(true, true).addClass('jbButtons');
            content.find('.order-sub-btn').click((e) => {
                elem.find('.order-sub-btn').trigger("click");
            });
            content.find('.order-back-btn').text(' powrót');
            jQuery('#form-order').after(content);
        });
    }
}
const orderModifierObject = new OrderModifier();
export default orderModifierObject;