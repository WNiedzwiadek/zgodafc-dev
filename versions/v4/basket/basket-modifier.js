import { ModifierSubject } from '../../../modifiers/modifierSubject'
import { awaitElements } from '../../../utils/elements';
import { getDeliveryDate } from '../../v1/utils/return-delivery-date';
import { executeScriptWithTag } from '../../../utils/executeScript';
import { findProperScriptTagWithDatalayer } from '../../../utils/parse-script-tag-products';
import guard from '../../../utils/guard';
import { ajaxSubject } from '../../../utils/ajax.subject';

class KpModifier extends ModifierSubject {
    constructor() {
        super('.basket', (that) => {})
    }

    applyChanges() {
        this.addFreeDeliveryInfo();
        this.watchBasketUpdate();
        this.handleVisibilityOfPromoCOde();
    }

    applyStyles() {
        require('./basket.scss');
    }

    watchBasketUpdate() {
        ajaxSubject.subscribe((url) => {
            if (/basket\/qty/.test(url)) {
                setTimeout(() => {
                    this.addFreeDeliveryInfo();
                });
            }
        });
    }

    findProperScript() {
        return new Promise((resolve) => {
            findProperScriptTagWithDatalayer((productArrayString) => {
                executeScriptWithTag(productArrayString).then(() => {
                    resolve(window.productInBasket);
                })
            });
        })
    }

     /** */
     handleVisibilityOfPromoCOde() {
        return awaitElements('.basket_discount .voucher_code').then((elem) => {
            elem.addClass('jbHidden');
            const jbRabatCodeObj = jQuery(`<span class="jbRabatCodeShow">Posiadam kod rabatowy</span>`);
            jbRabatCodeObj.click((e) => {
                jQuery('.jbRabatCodeShow').addClass('jbHidden');
                jQuery(`.basket_discount .voucher_code`).removeClass(`jbHidden`);
            })
            elem.after(jbRabatCodeObj);
        })
    }

    addFreeDeliveryInfo(){
        return awaitElements('.summary__final-price').then(elem => {
            const totalPrice = elem.attr('data-sum');//.text().replace(",", ".").replace(/[^0-9\.]+/g, "");

            if (totalPrice >= 200.00) {
                jQuery('.summary__delivery-price span').text('DARMOWY');
            }else{
                jQuery('.summary__delivery-price span').text('OD 9 PLN');
            }
        });
    }
}
const kpModifierObject = new KpModifier();
export default kpModifierObject;