import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';
import callInsertElement from '../../../utils/callInsert';
import HeaderObject from '../header/header-modifier';
import FooterObject from '../footer/footer-modifier';

class LoginModifier extends ModifierSubject {
    constructor() {
        super('.basket-summary', (that) => {});
    }

    applyChanges() {
        HeaderObject.addHeader();
        HeaderObject.addSSL();
        HeaderObject.addHeaderText('PODSUMOWANIE');
        HeaderObject.applyStyles();
        FooterObject.addFooter();
        FooterObject.applyStyles();

        this.addFreeDeliveryInfo();
    }

    applyStyles() {
        require('./confirm.scss');
    }

    addFreeDeliveryInfo() {
        return awaitElements('.summary__final-price').then(elem => {
            const totalPrice = elem.attr('data-sum');//.text().replace(",", ".").replace(/[^0-9\.]+/g, "");

            if (totalPrice >= 200.00) {
                jQuery('.summary__delivery-price span').text('DARMOWY');
            }else{
                jQuery('.summary__delivery-price span').text('OD 9 PLN');
            }
        });
    }
}

const loginModifierbject = new LoginModifier();
export default loginModifierbject;