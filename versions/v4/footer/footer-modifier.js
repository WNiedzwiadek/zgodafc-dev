import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';
import FooterTemplate from './footer-template.html';

class FooterModifier extends ModifierSubject {
    constructor() {
        super('.login_page.container', (that) => {});
    }

    applyStyles() {
        require('./footer.scss');
    }

    addFooter(){
        return awaitElements('.col-md-12.footer_options').then(elem => {
            elem.before(FooterTemplate);
        });
    }
}
const footerModifier = new FooterModifier();
export default footerModifier;