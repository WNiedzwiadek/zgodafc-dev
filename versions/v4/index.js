import {LoadingScreen} from "../../utils/loading-screen";
import {Device} from "../../utils/device";
import runGlobalPage from "./globalPage";

export default function run(){
    let device = new Device();
    device.setAllowed('mobile');
    window.runningVersionSSarr = window.runningVersionSSarr || [];
    if (device.isValid() && !~window.runningVersionSSarr.indexOf('v4')){
        console.log("dziala wersja v4");
        window.runningVersionSS = 'v4'; //for page quasii full reloading checking
        window.runningVersionSSarr.push('v4');
        const ls = new LoadingScreen();
        runGlobalPage();
    }
}