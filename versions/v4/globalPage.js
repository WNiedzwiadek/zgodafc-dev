import basketObject from './basket/basket-modifier';
import loginObject from './login/login-modifier';
import orderObject from './order/order-modifier';
import pathChecker from '../../utils/pathChecker';
import confirmObject from './confirm/confirm-modifier';
import deliveryObject from './delivery/delivery-modifier';
import kpObject from './kp/kp-modifier';

export default function runGlobalPage() {

    kpObject.subscribe(() => {
        kpObject.applyStyles();
        kpObject.applyChanges();
    })

    basketObject.subscribe(() => {
        basketObject.applyChanges();
        basketObject.applyStyles();
    })

    pathChecker(/login/, () => {
        loginObject.subscribe(() => {
            loginObject.applyChanges();
            loginObject.applyStyles();
        });
    })

    pathChecker(/order/, () => {
        orderObject.subscribe(() => {
            orderObject.applyChanges();
            orderObject.applyStyles();
        })
    });

    pathChecker(/order\/delivery/, () => {
        deliveryObject.applyChanges();
        deliveryObject.applyStyles();
    });

    pathChecker(/order\/confirm/, () => {
        confirmObject.subscribe(() => {
            confirmObject.applyChanges();
            confirmObject.applyStyles();
        })
    });

}