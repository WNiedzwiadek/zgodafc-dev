import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';
import callInsertElement from '../../../utils/callInsert';
import ProgressBarObject from '../progressbar/progressbar-modifier';
import HeaderObject from '../header/header-modifier';
import FooterObject from '../footer/footer-modifier';

class LoginModifier extends ModifierSubject {
    constructor() {
        super('.basket', (that) => {});
    }

    applyChanges() {
        ProgressBarObject.addProgressBar(2,'.container-fluid.down_margin', false);
        HeaderObject.addHeader();
        HeaderObject.addHeaderText('LOGOWANIE');
        HeaderObject.addSSL();
        HeaderObject.applyStyles();
        FooterObject.addFooter();
        FooterObject.applyStyles()
        this.addSectionHeader();
        this.addContWithoutRegText();
        //this.addLoginHeaderText();
        this.addContWithoutRegContentText();
        this.addRegUlContent();
    }

    applyStyles() {
        require('./login.scss');
    }

    addLoginHeaderText(){
        return awaitElements('.container-fluid.down_margin').then(elem => {
            elem.before('<span class=jbLoginHeader>LOGOWANIE</span>');
        });
    }

    addSectionHeader(){
        return awaitElements('.col-sm-12 .login-section__nologin').then(elem => {
            elem.before('<h2 class="page-header">SZYBKIE ZAKUPY BEZ REJESTRACJI</h2>');
        })
    }

    addContWithoutRegText(){
        return awaitElements('.small.text-muted').then(elem => {
            elem.after('<span class=jbContText>Kontynuj bez rejestrowania się</span>');
        });
    }

    addContWithoutRegContentText(){
        return awaitElements('.small.text-muted').then(elem => {
            elem.before('<p class=jbNoLoginText>Uwaga! Specjalnie dla naszych klientów uruchomiliśmy możliwość dokonania zakupu bez rejestracji. Pamiętaj jednak, że dokonując zamówienia bez rejestracji na stronie tracisz możliwość dostępu do historii zamówień, czy też statusu realizacji zamówienia, a także skracasz proces zakupowy (adres zamówienia oraz dane są dostępne na koncie).</p>');
        })
    }

    addRegUlContent(){
        return awaitElements('.register-section h3 + ul').then(elem => {
            elem.before(`<ul class=jbRegUlContent>
            <li>Historia zakupów w jednym miejscu.</li>
            <li>Możliwość sprawdzenia statusu realizacji zamówienia.</li>
            <li>Dostęp do faktury online.</li>
            <li>Możliwość zlecenia zwrotu.</li>
            </ul>`);
        });
    }
}

const loginModifierbject = new LoginModifier();
export default loginModifierbject;