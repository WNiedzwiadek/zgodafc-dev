import {LoadingScreen} from "../../utils/loading-screen";
import {Device} from "../../utils/device";
import runGlobalPage from "./globalPage";

export default function run(){
    let device = new Device();
    device.setAllowed('desktop');
    window.runningVersionSSarr = window.runningVersionSSarr || [];
    if (device.isValid() && !~window.runningVersionSSarr.indexOf('v2')){
        console.log("dziala wersja v2");
        window.runningVersionSS = 'v2'; //for page quasii full reloading checking
        window.runningVersionSSarr.push('v3');
        const ls = new LoadingScreen();
        runGlobalPage();
    }
}