import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';
import callInsertElement from '../../../utils/callInsert';
import ProgressBarObject from '../progressbar/progressbar-modifier';
import HeaderObject from '../header/header-modifier';
import FooterObject from '../footer/footer-modifier';

class DeliveryModifier extends ModifierSubject {
    constructor() {
        super('body', (that) => {});
    }

    applyChanges() {
        this.addNewDeliveryTitleText().then(()=>{
            ProgressBarObject.addProgressBar(2, '.jbNewDeliveryTitle', false);

            HeaderObject.addHeader();
            HeaderObject.addSSL();
            HeaderObject.applyStyles();
            FooterObject.addFooter();
            FooterObject.applyStyles();
            this.eventsDelivery();
            this.eventsPayments();
            this.addNewPaymentsTitleText();
            this.insertDeliveryHeaders();
            this.insertPaymentsWhiteSpace();
            this.insertPaymentsTooltips();
        })
    }

    applyStyles() {
        require('./delivery-modifier.scss');
    }
    addNewDeliveryTitleText(){
        return awaitElements('.top-bar__bg').then((elem)=>{
            elem.after(`
            <div class="row jbNewDeliveryTitle">
                <div class="container">
                    <h2>DOSTAWA I PŁATNOŚĆ</h2>
                </div>
            </div>`);
        })
    }

    addNewPaymentsTitleText(){
        return awaitElements('.row.payments').then(elem => {
            elem.before(`<h3 class=jbPaymentsHeader>WYBIERZ METODĘ PŁATNOŚCI:</h3>`);
        });
    }

    insertDeliveryHeaders() {
        return awaitElements('.row.deliveries label').then(elem => {
            elem.each((index, object) => {
               const obj = this.jQuery(object);
               obj.find('img').after(`<span class=jbWhiteSpace></span>`);
            }) 
        });
    }

    insertPaymentsWhiteSpace() {
        return awaitElements('.row.payments .payments__row label').then(elem => {
            elem.each((index, object) => {
               jQuery(object).prepend(`<span class=jbWhiteSpace></span>`);
            }) 
        });
    }

    insertPaymentsTooltips(){
        return awaitElements('.row.payments .payments__row label > span').then(elem => {
            elem.each((index, object) => {
               jQuery(object).before(`<img class="jbTooltip"></img>`);
            }) 
        });
    }

    eventsDelivery(){
        return awaitElements('.row.deliveries label').then(elem => {
            elem.click((e) => {
                const object = this.jQuery(e.currentTarget);
                this.jQuery('.jbDeliveryActive').removeClass('jbDeliveryActive');
                object.addClass('jbDeliveryActive');
            })
        });
    }

    eventsPayments(){
        return awaitElements('.row.payments .payments__row label').then(elem => {
            elem.click((e) => {
                const object = this.jQuery(e.currentTarget);
                this.jQuery('.jbPaymentsActive').removeClass('jbPaymentsActive');
                object.addClass('jbPaymentsActive');
            })
        });
    }
}

const deliveryModifierbject = new DeliveryModifier();
export default deliveryModifierbject;