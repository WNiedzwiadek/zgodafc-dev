import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';
import callInsertElement from '../../../utils/callInsert';
import AlertSize from './chose-size-alert.html';
import ModalBasketTemplate from './new-modal-template.html';
import { parseWholePage } from '../../../utils/parse-whole-page';
import TopInfoTemplate from './modal-top-info.html';

class KPModifier extends ModifierSubject {
    constructor() {
        super('.product', (that) => {});
    }

    applyChanges() {
        this.addModalTemplate().then(() => {
            this.addClickHandlerForAddCard();
        });
    }

    applyStyles() {
        require('./kp-modifier.scss');
    }

    addClickHandlerForAddCard() {
        return awaitElements('.product__action .btn.addToCart').then((elem) => {
            elem.off();
            this.rewriteAddToBasket();
            elem.click((e) => {
                e.preventDefault();
                this.handleAddtoBasket().then((isSizeChosed) => {
                    if (isSizeChosed) {
                        jQuery(".product__add").submit();
                    }
                })
            })
        })
    }

    rewriteAddToBasket() {
        const scope = this;
        jQuery(".product__add").submit(function(e) {
            e.preventDefault();
            jQuery.ajax({
                type: 'POST',
                url: `${jQuery(this).attr('action')}`,
                data: jQuery(this).serialize(),
                success: (data) => {
                    scope.fillModalTemplate();
                }
            });
        });
    }

    fillModalTemplate() {
        const modalAfterAdd = jQuery(`#afterAddTobasketModal`);
        const modalBody = modalAfterAdd.find('.modal-body');
        const isShoes = ~jQuery('.breadcrumb').text().indexOf('Buty');
        modalBody.html(`
            <div class="modal-mody-container">
                ${(isShoes) ? TopInfoTemplate  : ''}
                <div class="modal-mody-container-product">
                    <div class="modal-mody-container-product--image">
                        <img src="${jQuery('#tab-pictures .fotorama__img').attr('src')}">
                    </div>
                    <div class="modal-mody-container-product--info">
                        <span class="modal-mody-container-product--info--title">${jQuery('.product__header > h1').text()}</span>
                        <span class="modal-mody-container-product--info--code">${jQuery('.product__header > .product__code').text()}</span>
                        <span class="modal-mody-container-product--info--size">ROZMIAR: ${jQuery('.size__type .sizeType.active').text()} ${jQuery('.size > ul li.active .size_box').text()}</span>
                        <span class="modal-mody-container-product--info--price">${jQuery('.product__price .price__retail-value').text()}</span>
                    </div>
                </div>
            </div>
        `);
        modalAfterAdd.modal('show');
    }

    handleAddtoBasket() {
        return this.validateSize().then((validationResult) => {
            if (validationResult) {
                const productId = jQuery('.cart__action > a ').attr('data-itemid');
                addToCart(productId);
                return validationResult;
            }
        })
    }

    addModalTemplate() {
        return awaitElements('.container').then((elem) => {
            const modalTemplate=jQuery(ModalBasketTemplate);
            modalTemplate.find('.modal-footer--go-to-shoping').click((e)=>{
                jQuery('#afterAddTobasketModal .close').click();
            })
            modalTemplate.find('.modal-footer-go-to-basket').click((e)=>{
                window.location.href='https://www.zgodafc.pl/basket';
            })
            elem.append(modalTemplate);
        })
    }

    validateSize() {
        if (!jQuery('#size').attr('data-sizeeu') && !jQuery('#size').val()) {
            return awaitElements('.messages').then((elem) => {
                if(!jQuery('.jbSizeValidation').length){
                    elem.append(AlertSize);
                }
                return false;
            });
        } else {
            jQuery('.jbSizeValidation').remove();
            return Promise.resolve(true);
        }
    }
}

const kpModifierbject = new KPModifier();
export default kpModifierbject;