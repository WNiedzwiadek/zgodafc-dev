import BasketObject from './basket/basket-modifier';
import LoginObject from './login/login-modifier';
import OrderObject from './order/order-modifier';
import pathChecker from '../../utils/pathChecker';
import kpModifierbject from './kp-modifier/kp-modifier';
import DeliveryObject from './delivery/delivery-modifier';
import ConfirmObject from './confirm/confirm-modifier';

export default function runGlobalPage() {

    pathChecker(/basket/, () => {
        BasketObject.subscribe(() => {
            BasketObject.applyChanges();
            BasketObject.applyStyles();
        });
    })

    pathChecker(/login/, () => {
        LoginObject.subscribe(() => {
            LoginObject.applyChanges();
            LoginObject.applyStyles();
        });
    })

    pathChecker(/order/, () => {
        OrderObject.subscribe(() => {
            OrderObject.applyChanges();
            OrderObject.applyStyles();
        })
    });

    kpModifierbject.subscribe(() => {
        kpModifierbject.applyStyles();
        kpModifierbject.applyChanges();
    })

    pathChecker(/order\/delivery/, () => {
        DeliveryObject.applyChanges();
        DeliveryObject.applyStyles();
    });

    pathChecker(/order\/confirm/, () => {
        ConfirmObject.applyChanges();
        ConfirmObject.applyStyles();
    });
}