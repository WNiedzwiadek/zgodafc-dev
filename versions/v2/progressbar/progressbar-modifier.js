import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

import ProgressBar from './progressbar-template.html';

class ProgressBarModifier extends ModifierSubject {
    constructor() {
        super('.header_wrapper', (that) => {});
    }

    applyChanges(step = 1, selector = '#breadcrumbs', isLoginPage = false) {
        this.addProgressBar(step, selector, isLoginPage);
    }

    addProgressBar(step, selector, isLoginPage) {
        awaitElements(selector).then(element => {
            const result = (isLoginPage) ? jQuery(element).after(ProgressBar).after(`<div class="progress_bar_keeper progress_bar_2_active"></div>`) :
                jQuery(element).before(ProgressBar);
            this.setActiveStep(step);
        }).catch(error => console.log(error));
    }

    getProgressBar() {
        return jQuery(ProgressBar);
    }

    setActiveStep(stepNr) {
        let doneStep;
        let activeStep;
        let activeLine;
        let currentStep;
        switch (stepNr) {
            case 1:
                doneStep = jQuery('#jbStepNull');
                currentStep = jQuery('#jbStepOne');
                activeStep = jQuery('#jbStepOne');
                activeLine = jQuery('#jbLineOne');
                break;
            case 2:
                doneStep = jQuery('#jbStepOne');
                currentStep = jQuery('#jbStepTwo');
                activeStep = jQuery('#jbStepTwo');
                activeLine = jQuery('#jbLineOne, #jbLineTwo');
                break;
            case 3:
                doneStep = jQuery('#jbStepOne, #jbStepTwo');
                currentStep = jQuery('#jbStepThree');
                activeStep = jQuery('#jbStepThree');
                activeLine = jQuery('#jbLineOne, #jbLineTwo, #jbLineThree');
                break;
        }
        
        activeStep.removeClass('jbProgressInactiveStep').addClass('jbProgressActiveStep');
        activeLine.removeClass('jbProgressLineEmpty').addClass('jbProgressLineFilled');
        currentStep.addClass('jbProgressCurrentStep');
        currentStep.find('.jbProgressLabel').addClass('jbTextActive');

        if(doneStep.length > 0){
            doneStep.removeClass('jbProgressInactiveStep').addClass('jbProgressDoneStep');
        }
    }
}
const progressBarModifierObject = new ProgressBarModifier();
export default progressBarModifierObject;