import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';
import callInsertElement from '../../../utils/callInsert';
import ProgressBarObject from '../progressbar/progressbar-modifier';
import { ajaxSubject } from '../../../utils/ajax.subject';

class BasketModifier extends ModifierSubject {
    constructor() {
        super('.basket', (that) => {});
    }

    applyChanges() {
        ProgressBarObject.addProgressBar(1, '.messages', false);
        this.changeFreeDeliveryText();
        this.addDeliveryTextInBanner();
        this.handleVisibilityOfPromoCOde();
        this.watchBasketUpdate();
    }

    applyStyles() {
        require('./basket.scss');
    }

    watchBasketUpdate() {
        ajaxSubject.subscribe((url) => {
            if (/basket\/qty/.test(url)) {
                setTimeout(() => {
                    this.changeFreeDeliveryText();
                });
            }
        });
    }

    changeFreeDeliveryText() {
        return awaitElements('.summary__delivery-price span').then(elem => {
            const totalPrice = this.jQuery('.summary__final-price span').text().replace(",", ".").replace(/[^0-9\.]+/g, "");

            if (totalPrice >= 200.00) {
                elem.text('DARMOWY');
            }else{
                elem.text('OD 9 PLN');
            }
        })
    }

    addDeliveryTextInBanner() {
        return awaitElements('.sub-section.pl').then(elem => {
            elem.prepend(`<div class="title jbTitle">DARMOWA DOSTAWA OD 200ZŁ  </div>`)
        });
    }

    /** */
    handleVisibilityOfPromoCOde() {
        return awaitElements('.basket_discount .voucher_code').then((elem) => {
            elem.addClass('jbHidden');
            const jbRabatCodeObj = jQuery(`<span class="jbRabatCodeShow">Posiadam kod rabatowy</span>`);
            jbRabatCodeObj.click((e) => {
                jQuery('.jbRabatCodeShow').addClass('jbHidden');
                jQuery(`.basket_discount .voucher_code`).removeClass(`jbHidden`);
            })
            elem.after(jbRabatCodeObj);
        })
    }
}

const basketModifierbject = new BasketModifier();
export default basketModifierbject;