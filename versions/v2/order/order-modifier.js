import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';
import callInsertElement from '../../../utils/callInsert';
import ProgressBarObject from '../progressbar/progressbar-modifier';
import HeaderObject from '../header/header-modifier';
import FooterObject from '../footer/footer-modifier';

class LoginModifier extends ModifierSubject {
    constructor() {
        super('.order-address', (that) => {});
    }

    applyChanges() {
        ProgressBarObject.addProgressBar(2,'.container-fluid.down_margin', false);
        HeaderObject.addHeader();
        HeaderObject.addSSL();
        HeaderObject.addHeaderText('DANE DO WYSYŁKI');
        HeaderObject.applyStyles();
        FooterObject.addFooter();
        FooterObject.applyStyles()

        this.copyMailAddressContainer();
        this.copyBackButton();
        this.changeReqCheckboxText();
        this.fillOrygMailAddress();
    }

    applyStyles() {
        require('./order.scss');
    }

    addLoginHeaderText(){
        return awaitElements('.container-fluid.down_margin').then(elem => {
            elem.before('<span class=jbLoginHeader>DANE DO WYSYŁKI</span>');
        });
    }

    copyMailAddressContainer(){
        return Promise.all([
            awaitElements('.order-address > div:nth-child(4)'),
            awaitElements('#form-order > div:nth-child(3) > div')
        ]).then(([formElement, mailContainer]) => {
            formElement.append(mailContainer.clone(true, true).addClass('jbMail'));
        });
    }

    copyBackButton(){
        return awaitElements('.btn.btn-default.btn-gray.order-back-btn').then(elem => {
            elem.after('<a class="jbBack" href="https://www.zgodafc.pl/basket"><< powrót </a>');
        });
    }

    changeReqCheckboxText(){
        return awaitElements('label[for="agree"]').then(elem => {
            elem.html('(WYMAGANE) Zapoznałam/em się z <a href="https://www.zgodafc.pl/content/regulamin,132.html" target="_blank">Regulaminem sklepu internetowego</a> i akceptuję jego postanowienia.<br>');
        })
    }

    fillOrygMailAddress(){
        return awaitElements('.btn.btn-primary.pull-right.order-sub-btn').then(elem => {
            elem.click(()=>{
                const newMailValue = jQuery('.jbMail #email').val();
                jQuery('#form-order > div:nth-child(3) > div #email').val(newMailValue);
            });
        })
    }
}

const loginModifierbject = new LoginModifier();
export default loginModifierbject;