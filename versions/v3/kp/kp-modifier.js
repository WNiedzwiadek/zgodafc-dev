import { ModifierSubject } from '../../../modifiers/modifierSubject'
import { awaitElements } from '../../../utils/elements';
import { getDeliveryDate } from '../../v1/utils/return-delivery-date';

import PromotionTemplate from './promotion-template.html';

class KpModifier extends ModifierSubject {
    constructor() {
        super('.product', (that) => {})
    }
    applyChanges() {
        this.addPromotionInfo();
        this.moveSizeInfo();
        this.moveSoleInfo();
        this.selectSizeByDefault();
        this.addDeliveryInfo();
    }

    applyStyles() {
        require('./kp.scss');
    }

    addPromotionInfo(){
        return Promise.all([
            awaitElements('.product__body .product__percent'),
            awaitElements('.price__discount-value')
        ]).then(([percent, elementRef]) => {
            elementRef.after(PromotionTemplate);
        });
    }

    selectSizeByDefault() {
        return awaitElements('.size >ul >li:first-of-type').then((elem) => {
            elem.trigger('click');
        });
    }

    moveSizeInfo() {
        return Promise.all([
            awaitElements('.product__size-text'),
            awaitElements('.size__type')
        ]).then(([tableSizeElem, choseSizeText]) => {
            const sizeTable = jQuery(`<div class="jbSize"><p>WYBIERZ ROZMIAR</p><p class="product__size-text jbProdSize"><a href="#"><span style="font-size: 18px;" class="icon-measure"></span> Tabela rozmiarów</a></p></div>`);
            sizeTable.click((e) => {
                tableSizeElem.trigger('click')
            });
            choseSizeText.before(sizeTable);
        });
    }

    moveSoleInfo(){
        return Promise.all([
            awaitElements('.product__size-text'),
            awaitElements('.group_surface')
        ]).then(([sole, elementRef]) => {
            const content = jQuery(`<div class=jbSole><label>Rodzaj podeszwy</label><p class=jbSoleText>
                    <a href=https://www.zgodafc.pl/info/rodzaje-podeszw,152.html>Typy podeszw</a>
                    </p></div>`);
            elementRef.before(content);
        });
    }

    addDeliveryInfo(){
        awaitElements('.product__action').then((elem)=>{
            elem.append(`<span class=jbDelivery>${getDeliveryDate()}</span>`);
        })
    }
}
const kpModifierObject = new KpModifier();
export default kpModifierObject;