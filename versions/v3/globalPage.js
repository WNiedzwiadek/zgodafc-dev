import kpObject from './kp/kp-modifier';
import basketObject from './basket/basket-modifier';

export default function runGlobalPage() {
    kpObject.subscribe(() => {
        kpObject.applyChanges();
        kpObject.applyStyles();
    });

    basketObject.subscribe(() => {
        basketObject.applyChanges();
        basketObject.applyStyles();
    })
}