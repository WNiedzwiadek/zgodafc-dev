export function parseWholePage(data=''){
    const body = data.replace(/^[\s\S]*<body.*?>|<\/body>[\s\S]*$/ig, '');
    const div = document.createElement('div');
    div.innerHTML = body;
    return jQuery(div);
}