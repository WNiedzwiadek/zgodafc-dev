import { Subject } from "../subjects/subject";

class AjaxSubject extends Subject {
    constructor() {
        super();
        const oldXHR = window.XMLHttpRequest;

        const handleResponse = (xhr, whole)=>{
            this.notify(xhr, whole);
        };

        function newXHR() {
            var realXHR = new oldXHR();
            realXHR.addEventListener("readystatechange", function () {
                if (realXHR.readyState == 4 && (realXHR.status == 200 || realXHR.status == 204)) {
                    let url = this.responseURL;
                    if(!url && this.ea && this.ea.url){
                        url = this.ea.url;
                        if (!url){
                            url = this.url;
                        }
                    }
                    handleResponse(url, this);
                }
            }, false);
            return realXHR;
        }

        window.XMLHttpRequest = newXHR;
    }
}

export const ajaxSubject = new AjaxSubject();