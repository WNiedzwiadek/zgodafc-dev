export function executeScriptWithTag(jsToExecute='') {
    return new Promise((resolve)=>{
        const oScript = document.createElement("script");
        const oScriptText = document.createTextNode(jsToExecute);
        oScript.appendChild(oScriptText);
        document.head.appendChild(oScript);
        resolve(true);
    })
}