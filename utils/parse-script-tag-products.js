/**
 * Be sure that you execute this script after jQuery subscriber is called
 */
export function findProperScriptTagWithDatalayer(callback) {
    const script = jQuery('script').toArray()
        .find(singleScriptTag => ~singleScriptTag.innerText.indexOf('ecommerce'));
    if (script && script.innerText) {
        const scriptTagContent= script.innerText;
        try {
            const partialScriptTextWithProduct = scriptTagContent
                .slice(scriptTagContent.indexOf('products') - 1, scriptTagContent.length);
            const partialProductsArrayString =
                `window.productInBasket={${ partialScriptTextWithProduct.slice(0, partialScriptTextWithProduct.indexOf(']') + 1)}}`;
            callback(partialProductsArrayString);
        } catch (e) {
            console.log(e);
        }
    } else {
        setTimeout(() => {
            findProperScriptTagWithDatalayer(callback);
        }, 10)
    }
}