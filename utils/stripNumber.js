export function stripCurrency(input) {
    return input.replace(",", ".").replace(/[^1-9\.]+/g, "") + '0 zł';
}