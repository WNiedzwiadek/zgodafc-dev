const LoadExternalLibraries = () => {
    //check if object of loaded libraries exist
    const conversionLibraries = {};
    const loadOne = source => new Promise((resolve, reject) => {
        const scriptTag = document.createElement('script');
        // if previously loaded, resolve
        if (conversionLibraries[source]) {
            resolve();
        }
        conversionLibraries[source] = true;
        scriptTag.onerror = reject;
        scriptTag.onload = resolve;
        scriptTag.src = source;
        document.head.appendChild(scriptTag);
    });
    // handle multiple libraries lad
    const loadMultiple = sources => Promise.all(sources.map(source => loadOne(source)));

    return {
        loadOne,
        loadMultiple
    };
};

export const LoadLibraries = LoadExternalLibraries();