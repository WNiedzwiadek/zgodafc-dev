import {jQuerySubject} from '../subjects/jQuery.subject';

export class LoadingScreen{
    constructor(){
        document.getElementsByTagName('html')[0].style.visibility='hidden';
        jQuerySubject.subscribe(() => {
            jQuery(document).ready(()=>{
                setTimeout(() => {
                    document.getElementsByTagName('html')[0].style.visibility='visible';
                }, 25);
            });
                
        });
    }

    hide() {
        jQuerySubject.subscribe(() => {
            setTimeout(() => {
                document.getElementsByTagName('html')[0].style.visibility='visible';
            }, 25);
        });
    }
}