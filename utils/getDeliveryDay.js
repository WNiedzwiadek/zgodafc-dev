export default class DeliveryDataCompute {
    constructor() {
        this.currentYear = new Date().getFullYear();
        this.daysMillisecondsIncrease = 86400000;
        this.holidaysList = [
            new Date(this.currentYear, 0, 1),
            new Date(this.currentYear, 0, 6),
            new Date(this.currentYear, 4, 1),
            new Date(this.currentYear, 4, 3),
            new Date(this.currentYear, 5, 4),
            new Date(this.currentYear, 7, 15),
            new Date(this.currentYear, 10, 1),
            new Date(this.currentYear, 10, 11),
            new Date(this.currentYear, 11, 25),
            new Date(this.currentYear, 11, 26)
        ];
        this.fillHolidays();

    }
    fillHolidays() {
        const easter = this.computeEaster(this.currentYear)
        const easterMon = new Date(Number(easter.getTime()) + 86400000);
        const godlyBody = new Date(Number(easter.getTime()) + 60 * 86400000);
        this.holidaysList.push(easter);
        this.holidaysList.push(easterMon);
        this.holidaysList.push(godlyBody);
    }

    skipWorkingDays(days = 0, beginDate = new Date(), useFreeDays = true, useHolidays = true) {
        let dateAfterDays = new Date(beginDate);
        for (let i = 0; i<=days; i++){
            dateAfterDays.setTime(this.getNextWorkingDay(dateAfterDays,useFreeDays,useHolidays).getTime());
            if (i<days){
                dateAfterDays.setTime(dateAfterDays.getTime() + this.daysMillisecondsIncrease);
            }
        }
        return this.getNiceDateObject(dateAfterDays);
    }

    nextWorkingDays(days = 0, beginDate = new Date(), useFreeDays = true, useHolidays = true) {
        let dateAfterDays = new Date(beginDate);
        dateAfterDays.setTime(dateAfterDays.getTime() + this.daysMillisecondsIncrease * days);
        return this.getNiceDateObject(this.getNextWorkingDay(dateAfterDays, useFreeDays, useHolidays));
    }

    getNextWorkingDay(dateAfterDays, useFreeDays, useHolidays) {
        return this.checkIfIsWorking(dateAfterDays, useFreeDays, useHolidays);
    }

    checkIfIsWorking(date, useFreeDays, useHolidays) {
        if (useFreeDays && useHolidays) {
            if (!this.isFreeDay(date) && !this.isHoliday(date)) {
                return date;
            } else {
                return this.checkNextDay(date, useFreeDays, useHolidays);
            }
        } else if (useFreeDays) {
            if (!isFreeDay(date)) {
                return date;
            } else {
                return this.checkNextDay(date, useFreeDays, useHolidays);
            }
        } else if (useHolidays) {
            if (!isHoliday(date)) {
                return date;
            } else {
                return this.checkNextDay(date, useFreeDays, useHolidays);
            }
        }
    }
    checkNextDay(date, useFreeDays, useHolidays) {
        date.setTime(date.getTime() + this.daysMillisecondsIncrease);
        return this.checkIfIsWorking(date, useFreeDays, useHolidays);
    }

    isFreeDay(date) {
        return date.getDay() == 6 || date.getDay() == 0;
    }

    isHoliday(date) {
        for (let i = 0; i < this.holidaysList.length; i++) {
            if (this.holidaysList[i].getDate() == date.getDate() && this.holidaysList[i].getMonth() == date.getMonth()) return true;
        }
        return false;
    }

    getNiceDateObject(date) {
        return {
            day: `${date.getDate()}`.padStart(2, 0),
            month: `${date.getMonth() + 1}`.padStart(2, 0),
            year: date.getFullYear(),
            dayOfWeek:date.getDay()
        }
    }

    getDayMap(dayofWeek){
        const days=['w niedziele','w poniedziałek','we wtorek','w środę','w czwartek','w piątek','w sobotę'];
        return days[dayofWeek];
    }

    computeEaster(Y) {
        const C = Math.floor(Y / 100);
        const N = Y - 19 * Math.floor(Y / 19);
        const K = Math.floor((C - 17) / 25);
        let I = C - Math.floor(C / 4) - Math.floor((C - K) / 3) + 19 * N + 15;
        I = I - 30 * Math.floor((I / 30));
        I = I - Math.floor(I / 28) * (1 - Math.floor(I / 28) * Math.floor(29 / (I + 1)) * Math.floor((21 - N) / 11));
        let J = Y + Math.floor(Y / 4) + I + 2 - C + Math.floor(C / 4);
        J = J - 7 * Math.floor(J / 7);
        const L = I - J;
        const M = 3 + Math.floor((L + 40) / 44);
        const D = L + 28 - 31 * Math.floor(M / 4);
        return new Date(Y, M - 1, D);
    }

}