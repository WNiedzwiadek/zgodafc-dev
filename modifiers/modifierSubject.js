import {jQuerySubject} from '../subjects/jQuery.subject';
import {Subject} from '../subjects/subject';
export class ModifierSubject extends Subject {
    constructor(selector, callback) {
        super();
        jQuerySubject.subscribe((jQuery)=>{
            this.jQuery = jQuery;
            jQuery(document).ready(()=>{
                this.domReady = true;
            });
            this.wait(selector,callback);
        });
    }

    wait(selector, callback){
        this.modifiedElement = this.jQuery(selector);
        if(this.modifiedElement.length === 0){
            if(!this.domReady){
                setTimeout(()=>{
                    this.wait(selector,callback);
                }, 100);
            }
        } else {
            setTimeout(()=>{
                if (callback) {
                    callback(this);
                }
                this.notify();
            },0);
            
        }
    }
}