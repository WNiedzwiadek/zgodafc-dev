'use strict';

module.exports = {
    module: {
        rules: [{
                test: /\.js/,
                loader: 'babel-loader?presets[]=es2015'
            },
            {
                test: /\.html/,
                loader: 'html-loader'
            }, 
            {
                test: /\.png/,
                loader: 'url-loader'
            },
            {
                test: /\.svg/,
                loader: 'url-loader'
            },
            {
                test: /\.less$/,
                loader: "style-loader!css-loader!less-loader"
            },
            {
                test: /\.scss$/,
                loader: "style-loader!css-loader!sass-loader"
            }
        ]
    }
};
