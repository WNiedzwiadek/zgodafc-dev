'use strict';

function runRandom() {
    //its fill by random-init command from swiftswap-cli
    var deviceConfig = {
        defaultVersionIndex: 0, //version to return when device is not in allowed in array, -1 means that shows original page
        mobile: {
            originVersion: 0, // default origin version for selected device
        },
        tablet: {
            originVersion: 0, // default origin version for selected device
        },
        desktop: {
            originVersion: 0, // default origin version for selected device
        }
    }

    var globalConfig = {
        /*CLIENT.ID*/

        testSpecificId: '20181204.ZGODAFC',
        testName : '20181204 v0-oryg, v1 -v1, v2-v2, v3-v3, v4-v4',
        COOKIE_TIME_PERIOD: 182.5,
        cnv_device_allowed: ['desktop','mobile'],
        current_device: '',
        domain: '.zgodafc.pl',
        waitForEntryPoint: false,
        stopwaitForjQuery: false,
        varient: 5,
        mobile: {
            entryPoint: 'body',
            versionArray: [deviceConfig.mobile.originVersion, 4], //id of selected versions
            versionProbabilty: [0.5, 0.5] //probability of chose version
        },
        tablet: {
            entryPoint: 'body',
            versionArray: [deviceConfig.tablet.originVersion, 1, 2], //id of selected versions
            versionProbabilty: [0.25, 0.25, 0.25] //probability of chose version
        },
        desktop: {
            entryPoint: 'body', //string or regex for page
            versionArray: [deviceConfig.desktop.originVersion], //id of selected versions
            versionProbabilty: [1] //probability of chose version
        }
    }


    /**
     * Refactored device check.
     * @param targeted_devices
     * @example ['desktop', 'mobile']
     * @returns {number}
     */
    function checkDevice(targeted_devices) {
        function isMobileAndTablet() {
            var check = false;
            (function(a) { a = a.toLowerCase(); if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
            return check ? "tablet" : "desktop";
        }

        function isMobile() {
            var check = false;
            (function(a) { a = a.toLowerCase(); if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
            return check ? "mobile" : false;
        }
        // TODO: config.current_device assign
        globalConfig.current_device = isMobile() || isMobileAndTablet();
        console.log(globalConfig.current_device);
    }

    /**
     * Refractored roll version with device check
     * @returns {number}
     */
    function rollVersion() {
        function getProbableRandom(results, weights) {
            var num = Math.random(),
                sum = 0,
                lastIndex = globalConfig[globalConfig.current_device].versionArray.length - 1;
            for (var i = 0; i < lastIndex; ++i) {
                sum += weights[i];
                if (num < sum) {
                    return results[i];
                }
            }
            return results[lastIndex];
        };

        var actualDeviceIndex = globalConfig.cnv_device_allowed.indexOf(globalConfig.current_device);
        var currentDeviceObject = globalConfig[globalConfig.current_device];
        return (actualDeviceIndex == -1) ? globalConfig.varient :
            getProbableRandom(currentDeviceObject.versionArray, currentDeviceObject.versionProbabilty);
    }
    /**
     * Set cookie for chosed page
     * @param cname
     * @param cvalue
     * @param exdays
     */

    function setCookie(cname, cvalue, exdays) {
        exdays = exdays || globalConfig.COOKIE_TIME_PERIOD;
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/; domain=" + globalConfig.domain + ";";
    }

    /**
     * Get cookie for chosed page
     * @param cname
     * @returns {cookieValue}
     */
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    /**
     * Parse dec value to hex value
     * @param number
     * @returns {parsedValue}
     */
    function getHexStr(number) {
        return number.toString(16);
    }

    /**
     * Parse hex value to devvalue
     * @param hexStr
     * @returns {parsedValue}
     */
    function getDecNumber(hexStr) {
        return parseInt(hexStr, 16);
    }

    /**
     * Wait for
     * @param Function or promise
     * @returns {versionId}
     */
    function waitForJQuery(callback) {
        if (!window.jQuery) {
            setTimeout(function() {
                waitForJQuery(callback);
            }, 25);
        } else callback();
    }


    /**
     * Get version index, if user has cookie set return previosly cookie version, if not get random id version
     * @param Function or promise
     * @returns {versionId}
     */
    function getVersion(callback) {
        var cnvCookie = getCookie("_cnveoexp");
        checkDevice(globalConfig.cnv_device_allowed);
        if (cnvCookie !== "" && !!~cnvCookie.indexOf(globalConfig.testSpecificId)) {
            return callback(getCookieVersion(cnvCookie));
        } else {
            var rolledVersion = getHexStr(rollVersion());
            var cookieData = globalConfig.testSpecificId + "-" + rolledVersion;

            if (globalConfig.waitForEntryPoint) {
                waitForJQuery(function() {
                    jQuery(document).ready(function() {
                        globalConfig.stopwaitForjQuery = true;
                    });
                    (function waitForElement() {
                        if (jQuery(globalConfig[globalConfig.current_device].entryPoint).length > 0) {
                            setCookie("_cnveoexp", cookieData);
                            console.log('wersja', rolledVersion);
                            callback(rolledVersion);
                        } else if (!globalConfig.stopwaitForjQuery) {
                            setTimeout(function() {
                                waitForElement();
                            }, 25);
                        } else {
                            //default version
                            callback(globalConfig.varient);
                        }
                    })();
                });
            } else {
                callback(getDirectlyVersion(rolledVersion, cookieData));
            }
        }
    }

    /**
     * Get version index, if user has cookie set return previosly cookie version, if not get random id version
     * @param Function or promise
     * @returns {versionId}
     */
    function getCookieVersion(cookieData) {
        var i;
        for (i = 0; i < globalConfig.varient; i++) {
            if (cookieData == globalConfig.testSpecificId + "-" + getHexStr(i)) {
                console.log('wersja', getHexStr(i));
                return getHexStr(i);
            }
        }
        console.log('wersja', globalConfig.varient);
        return globalConfig.varient;
    }

    function getDirectlyVersion(rolledVersion, cookieData) {
        if (rolledVersion >= globalConfig.varient) {
            return globalConfig.varient;
        } else {
            setCookie("_cnveoexp", cookieData);
            console.log('wersja', rolledVersion);
            return rolledVersion;
        }
    }

    function cnv_gtm_tracking() {
        var cookie_version = cnveoexp.slice(cnveoexp.indexOf(globalConfig.testSpecificId) + globalConfig.testSpecificId.length + 1,
            cnveoexp.indexOf(globalConfig.testSpecificId) + globalConfig.testSpecificId.length + 2);
        cookie_version = parseInt(cookie_version, 16) < globalConfig.varient ? cookie_version : 'nie-wylosowany';
        return 'test: ' + globalConfig.testName + ' - wersja: ' + cookie_version;
    }

    //serwowanie zmian
    var cnveoexp;

    getVersion(function(version) {
        //śledzenie zmian na każdej stronie na której ktoś ma ciacho z danego testu
        cnveoexp = getCookie("_cnveoexp");
        if (cnveoexp !== "" && cnveoexp.indexOf(globalConfig.testSpecificId) != -1) {
            window.cnv_gtm_tracking = cnv_gtm_tracking;
            window.cnvrsn_wersja_testu = window.cnv_gtm_tracking();
        }
        if (globalConfig.cnv_device_allowed.indexOf(globalConfig.current_device) != -1) { //czy dobra strona? oraz czy device na którym chcemy testować
            var versionSetting = {};         
            switch (getDecNumber(version)) {
                case 0:
                    versionSetting.id = deviceConfig[globalConfig.current_device].originVersion;
                    versionSetting.message = "Działa wersja oryginalna";
                    versionXChanges(versionSetting);
                    break;
                case 1:
                    versionSetting.id = 1;
                    versionSetting.message = "Działa wersja v1 (v1)";
                    versionXChanges(versionSetting);
                    break;
                case 2:
                    versionSetting.id = 2;
                    versionSetting.message = "Działa wersja v2 (v2)";
                    versionXChanges(versionSetting);
                    break;
                case 3:
                    versionSetting.id = 3;
                    versionSetting.message = "Działa wersja v3 (v3)";
                    versionXChanges(versionSetting);
                    break;
                case 4:
                    versionSetting.id = 4;
                    versionSetting.message = "Działa wersja v4 (v4)";
                    versionXChanges(versionSetting);
                    break;
                default:
                    versionSetting.id = 0;
                    versionSetting.message = "Działa wersja oryginalna (niewylosowana)";
                    versionXChanges(versionSetting);
                    break;
            };
        }
    });



    function versionXChanges(settings) {
        /*runSwiftswapVersionStart*//*runSwiftswapVersionEnd*/
        window.runSwiftswapVersion(settings.id);
        console.log(settings.message);
    }

}



if (history.state != null && history.state.errorConv != undefined) {
    window.conversionScriptError = true;
    history.replaceState(null, "");
} else {
    window.conversionScriptError = false;
    try {
        runRandom();
    } catch (err) {
        console.log("Conv throw error");
        history.pushState({ errorConv: true }, "errorConv");
        history.go(0);
    }
}

if (typeof window.cnv_gtm_tracking === 'function') {
    var cnvrsn_wersja_testu = window.cnv_gtm_tracking();
}